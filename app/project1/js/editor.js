
var toolTipApp = angular.module('toolTipApp',[]);

toolTipApp.controller('ToolTipCtrl',function($scope){
    $scope.showToolTip =false;
    $scope.myInput ="hallo world";
    $scope.hideToolTip = function(){
        console.log('here in hide');
        $scope.showToolTip=false;
    };
    $scope.toogleToolTip = function($event){
        event.stopPropagation();
        console.log('here in toogle');
        $scope.showToolTip = !$scope.showToolTip;
    };

});