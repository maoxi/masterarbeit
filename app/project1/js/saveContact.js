var test2App = angular.module('test2App',['ngRoute']);

test2App.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: 'partials/noDetail.html',
            controller: 'ContactController'
        })
        .when('/viewDetail',{
            templateUrl: 'partials/viewDetail.html',
            controller: 'ContactController'
        })
        .otherwise({redirectTo: '/'});
}]);


var controllers={};
controllers.ContactController = function($scope, ContactService) {
    $scope.contacts = ContactService.list();

    $scope.saveContact = function(){
        ContactService.save($scope.newContact);
        $scope.newContact={};
    };

    $scope.deleteContact = function( id ){
        ContactService.delete(id);
    };
    $scope.editContact = function( id ){
        $scope.newContact = angular.copy(ContactService.get(id));
    };
};

test2App.service('ContactService', function () {
    //to create unique contact id
    var uid = 1;

    //contacts array to hold list of all contacts
    var contacts = [{
        id: 0,
        'name': 'Viral',
        'email': 'hello@gmail.com',
        'phone': '123-2343-44'
    }];

    //save method create a new contact if not already exists
    //else update the existing object
    this.save = function (contact) {
        if (food.id == null) {
            //if this is new contact, add it in contacts array
            food.id = uid++;
            contacts.push(food);
        } else {
            //for existing contact, find this contact using id
            //and update it.
            for (i in contacts) {
                if (contacts[i].id == food.id) {
                    contacts[i] = food;
                }
            }
        }

    };

    //simply search contacts list for given id
    //and returns the contact object if found
    this.get = function (id) {
        for (i in contacts) {
            if (contacts[i].id == id) {
                return contacts[i];
            }
        }

    };

    //iterate through contacts list and delete
    //contact if found
    this.delete = function (id) {
        for (i in contacts) {
            if (contacts[i].id == id) {
                contacts.splice(i, 1);
            }
        }
    };

    //simply returns the contacts list
    this.list = function () {
        return contacts;
    }
});

test2App.controller(controllers);