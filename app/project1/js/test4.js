var test4App = angular.module('test4App', []);

test4App.controller('MyCtrl', function ($scope) {
    $scope.buttonname = "I'm a button";

    $scope.showChore = function (message) {
        alert(message + " is done! ");
    };
});

test4App.directive('kid', function () {
    return {
        restrict: "E",
        scope: {
            done: "&"
        },
        template: "<input type='text' ng-model='chore'>" + "<button ng-click='done({message: chore})'> I\'m done </button>  "
    }
});



// turn on Debug
test4App.run(function ($rootScope, $log) {
    $rootScope.$log = $log;
});

//turn off debug
test4App.config(function ($logProvider) {
    $logProvider.debugEnabled(false);
});

