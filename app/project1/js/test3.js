var test3App = angular.module('test3App',[]);

test3App.controller('MyCtrl',function($scope){
    console.log($scope);
});

test3App.directive('helloWorld',function(){
    return {
        scope:{},
        restrict:'A',
        link: function(scope, element, attrs){
            console.log(scope);
            scope.greeting ='hello world';
        },
        template: '<div>{{greeting}}</div>'
    }
});

