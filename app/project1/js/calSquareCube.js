var test1App = angular.module('test1App',[]);
var controllers={};
controllers.CalController = function($scope, CalculatorService) {
    $scope.doSquare = function(){
        $scope.result = CalculatorService.square($scope.value);
    };
    $scope.doCube = function(){
        $scope.result = CalculatorService.cube($scope.value);
    }
};

test1App.service('MathService', function(){
    this.add = function (a,b) {
        return a+b;
    };
    this.substract = function(a,b){
        return a-b;
    };
    this.multiply = function(a,b){
        return a*b;
    }
    this.divide = function(a,b){
        return a/b;
    }
});
test1App.service('CalculatorService',function(MathService){
    this.square = function(a){
        return MathService.multiply(a,a);
    };
    this.cube = function(a){
        return MathService.multiply(MathService.multiply(a,a),a);
    };
});

test1App.controller(controllers);