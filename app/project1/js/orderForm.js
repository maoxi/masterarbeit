var orderApp = angular.module('orderApp',[]);

orderApp.controller('orderCtrl',function($scope){
    $scope.priceList = [
        {name:'Web Development',price:300,active:false},
        {name:'Design',price:400, active:false},
        {name:'Integration',price:250, active:false},
        {name:'Training',price:220, active:false}];

    $scope.total = 0;

    $scope.calSum=  function(){
        $scope.total =0;
        angular.forEach($scope.priceList, function(pl){
            if(pl.active){
                $scope.total += pl.price;
            }
        });
    };

    $scope.toggleActive= function(pricePair){
        pricePair.active = !pricePair.active;
        $scope.calSum();
    }

});