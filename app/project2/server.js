Array.prototype.remove = function (element) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] === element) {
            this.splice(i, 1);
            return true;
        }
    }
    return false;
};

Array.prototype.contains = function (element) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] === element) {
            return true;
        }
        return false;
    }
};

Array.prototype.toggle = function (element) {
    if (!this.remove(element))
        this.push(element);
}

/**
 * Created by xinyao on 12.05.2015.
 */
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
server.listen(port,server_ip_address, function () {
    console.log("Listening on " + server_ip_address + ", server_port " + port)
});

/*
server.listen(port, function () {
    console.log('Server listening at port %d', port);
});
*/

var mongojs = require('mongojs');
var db = mongojs('foodList', ['foodList']);
var dbBooking = mongojs('booking', ['booking']);
var dbChat = mongojs('chat', ['chat']);
var bodyParser = require('body-parser');  //for json encode decode
var util = require('util');

//static source fold
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.get('/foodList', function (req, res) {
    console.log("I received a GET request");
    db.foodList.find(function (err, docs) {
        res.json(docs);
    });
});

app.get('/calendar', function (req, res) {
    dbBooking.booking.find(function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});

app.get('/chatroom',function(req,res){
    dbChat.chat.find(function(err,docs){
        res.json(docs);
    });
});

app.post('/foodList', function (req, res) {
    console.log(req.body);
    db.foodList.findOne({name: req.body.name}, function (err, result) {
        if (result) {
            var found = result;
            console.log(req.body);

            db.foodList.remove(found, function (err, doc) {
                console.log('error deleting in update');
            });
            db.foodList.insert(req.body, function (err, doc) {
                res.json(doc);
            });
        } else {
            db.foodList.insert(req.body, function (err, doc) {
                res.json(doc);
            });
        }
    });
});

app.post('/calendar/toggle', function (req, res) {
    dbBooking.booking.findOne({person: req.body.person}, function (err, result) {
        if (result) {
            result.days.toggle(req.body.day);
        }
        else {
            result = {
                person: req.body.person,
                days: [req.body.day]
            };
        }
        console.log(result);
        dbBooking.booking.save(result, function (err, doc) {
            res.json(doc);
        });
    });
});

app.post('/chatroom', function (req, res) {
        result = {
            person: req.body.person,
            days: [req.body.day]
        };
        dbChat.chat.save(result, function (err, doc) {
            res.json(doc);
        });
});

app.delete('/foodList/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    db.foodList.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.delete('/calendar/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    dbBooking.booking.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.get('/foodList/:id', function (req, res) {
    var id = req.params.id;
    db.foodList.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.get('/calendar/:id', function (req, res) {
    var id = req.params.id;
    dbBooking.booking.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

//for chatroom
var usernames = {};
var numUsers = 0;

io.on('connection',function(socket){
    var addedUser = false;

    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {
        // we tell the client to execute 'new message'
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data
        });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username) {
        // we store the username in the socket session for this client
        socket.username = username;
        // add the client's username to the global list
        usernames[username] = username;
        console.log(usernames);
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers,
            names: usernames
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers,
            names: usernames
        });
    });


    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        // remove the username from global usernames list
        if (addedUser) {
            delete usernames[socket.username];
            --numUsers;

            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers,
                names: usernames
            });
        }
    });
});