/**
 * Created by xinyao on 12.05.2015.
 */

barbecueApp.controller('FoodListCtrl', function ($scope, $http) {
    console.log('hallo world from controller');

    var refresh = function () {
        $http.get('/foodList').success(function (response) {
            console.log("I got this data");
            $scope.foodList = response;
            $scope.food = "";
        });
    };

    refresh();

    $scope.addFood = function () {
        console.log('addFood');
        $http.post('/foodList', $scope.food).success(function (res) {
            console.log(res);
            refresh();
        });
    };

    $scope.removeFood = function (id) {
        console.log(id);
        $http.delete('foodList/' + id).success(function (res) {
            console.log(res);
            refresh();
        });
    };

    $scope.editFood = function (someFood) {
        console.log(deepCopy(someFood)._id);
        var copiedFood= deepCopy(someFood);
        $scope.food= {name:copiedFood.name, food:copiedFood.food, quantity: copiedFood.quality};
    };
});
