
barbecueApp.controller('BbqPicsCtrl',['$scope','BbqPic',function($scope,BbqPic){
    $scope.bbqPics= BbqPic.query();
    $scope.usrSel='name';
}]);

barbecueApp.controller('BbqPicsDetailCtrl',['$scope','$routeParams','BbqPic',function($scope,$routeParams,BbqPic){
    $scope.aBbq = BbqPic.get({bbqPicId:$routeParams.bbqPicId},function(data){
        $scope.mainPic = data.images[0];
    });
    $scope.switchPic = function(pic){
        $scope.mainPic = pic;
    };
}]);