/**
 * Created by xinyao on 12.05.2015.
 */

barbecueApp.controller('ChatroomCtrl', function ($scope, $http, LoginData) {
    var connected = false;
    $scope.messages = [];

    //socket.emit('add user', LoginData.getUserName());

    $scope.sendMessage = function (event) {
        if (event.which === 13) {
            if ($scope.inputMessage && connected) {
                $scope.messages.push({username: LoginData.getUserName(), context: $scope.inputMessage, style: "myMsg"});
                $('.chatRoomMessages').animate({scrollTop: $('.inputMessage').offset().top }, "slow");
                $scope.socket.emit('new message', $scope.inputMessage);
                $scope.inputMessage = '';
            }
        }
    };

    function addParticipantsMessage(data) {
        var message = '';
        /*if (data.numUsers === 1) {
            $scope.messages.push({username: "notice", context: "孤独一人: "+JSON.stringify(data.names), style: "noticeMsg"});
        } else {*/
            $scope.messages.push({
                username: "notice",
                context: Object.keys(data.names).map(function(key){return data.names[key]})+" 还在",
                style: "noticeMsg"
            });
        //}
    }

    $scope.socket.on('login', function (data) {
        connected = true;
        addParticipantsMessage(data);
        $('.chatRoomMessages').animate({scrollTop: $('.inputMessage').offset().top }, "slow");
        $scope.$apply();
    });
    $scope.socket.on('new message', function (data) {
        //$scope.$apply(function () {
            $scope.messages.push({username: data.username, context: data.message, style: "othersMsg"});
        //});
        $('.chatRoomMessages').animate({scrollTop: $('.inputMessage').offset().top }, "slow");
        $scope.$apply();
    });
    $scope.socket.on('user joined', function (data) {
        //$scope.$apply(function () {
            $scope.messages.push({username: "notice", context: data.username + " joined", style: "noticeMsg"});
        //});
        addParticipantsMessage(data)
        $('.chatRoomMessages').animate({scrollTop: $('.inputMessage').offset().top }, "slow");
        $scope.$apply();
    });
    $scope.socket.on('user left', function (data) {
        //$scope.$apply(function () {
            $scope.messages.push({username: "notice", context: data.username + " left", style: "noticeMsg"});
        //});
        addParticipantsMessage(data);
        $('.chatRoomMessages').animate({scrollTop: $('.inputMessage').offset().top }, "slow");
        $scope.$apply();
    });
});

