'use strict';

var bbqPicServices = angular.module('bbqPicServices',['ngResource']);

bbqPicServices.factory('BbqPic',['$resource',function($resource){
    return $resource('meets/:bbqPicId.json',{},{
        query: {method:'GET',params:{bbqPicId:'meets'},isArray:true}  /*create a RESTful client, then be used instead of the lower-level $http service*/
    });
}]);